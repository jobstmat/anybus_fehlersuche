# Implementation of build process.
# Configured using variables defined in Makefile.
#
# supported:
#	- incremental build (including changes to headerfiles)
#	- out of tree build
#	- target platforms:
#		A53_Linux, R5_Baremetal
#	- target types:
#		EXECUTABLE, STATIC_LIBRARY, DYNAMIC_LIBRARY
#	- languages:
#		C/C++ (ASM could be added if required)
#
# build.mk should be identical in all projects.
# Changes to this file should be commitet to all projects.
# The date at the end of this header should be updated every time this file is changed.
#
# Copyright (c) 2021, Maschinenfabrik Reinhausen GmbH, Power Quality,
# Regensburg
# All Rights Reserved.
#
# author: Alexander Forster
# date:   04.01.2022
###############################################################################
# helper functions / tools

# determine operating system
PLATFORM := $(shell uname)

# set TARGET_TYPE to EXECUTABLE if not specified
TARGET_TYPE ?= EXECUTABLE

# platform specific settings for windows host PC
# CXX and CC are set automaticly in Linux
ifeq ($(PLATFORM),windows32)
	ifeq ($(TARGET_PLATFORM),A53_Linux)
		CXX := aarch64-linux-gnu-g++
		CC  := aarch64-linux-gnu-gcc
		# use gcc-ar instead of ar to allow link time optimization
		AR  := aarch64-linux-gnu-gcc-ar
	else ifeq ($(TARGET_PLATFORM),R5_Baremetal)
		CXX := armr5-none-eabi-g++
		CC  := armr5-none-eabi-gcc
		# use gcc-ar instead of ar to allow link time optimization
		AR  := armr5-none-eabi-gcc-ar
	else
		$(error target platform $(TARGET_PLATFORM) not supported)
	endif
else
	ifeq ($(TARGET_PLATFORM),R5_Baremetal)
		CXX := ${PETALINUX}/tools/linux-i386/gcc-arm-none-eabi-r5/bin/armr5-none-eabi-g++
		CC  := ${PETALINUX}/tools/linux-i386/gcc-arm-none-eabi-r5/bin/armr5-none-eabi-gcc
		# use gcc-ar instead of ar to allow link time optimization
		AR  := ${PETALINUX}/tools/linux-i386/gcc-arm-none-eabi-r5/bin/armr5-none-eabi-gcc-ar
	endif
endif

# use C++ compiler as linker
LD	:= $(CXX)

# select target type specific compiler/linker commands and options
ifeq ($(TARGET_TYPE),EXECUTABLE)
	LINKER_COMMAND = $(LD) -o $@ $(LDFLAGS) $^ $(addprefix -L,$(LD_PATHS)) $(addprefix -l,$(LDLIBS)) $(LDFLAGS_END)
else ifeq ($(TARGET_TYPE),STATIC_LIBRARY)
	LINKER_COMMAND = $(AR) rcs $@ $^
else ifeq ($(TARGET_TYPE),DYNAMIC_LIBRARY)
	LINKER_COMMAND = $(LD) -o $@ -shared $(LDFLAGS) $^ $(addprefix -L,$(LD_PATHS)) $(addprefix -l,$(LDLIBS)) $(LDFLAGS_END)
	DYNAMIC_LIB_ARG := -fPIC
else
	$(error invalid TARGET_TYPE: $(TARGET_TYPE))
endif

# compare string equality
# $(1)	first string
# $(2)	second string
# return empty if the strings are not equal, string content if the strings are equal
eq = $(and $(and $(findstring x$(1),x$(2)), $(findstring x$(2),x$(1))),$(1))

# Function that searches a given list of directories for files with defined extension
# $(1) list of file extensions
# $(2) list of search directories
# $(3) list of subdirectories (applied to each search directory)
# return list of all found files
FIND_SOURCE_FILES = $(strip															\
						$(foreach dir,$(2),											\
							$(foreach ext,$(1),										\
								$(wildcard $(dir)/*.$(ext))							\
								$(wildcard $(dir)/src/*.$(ext))						\
								$(foreach subDirName,$(3),							\
									$(wildcard $(dir)/$(subDirName)/*.$(ext))		\
									$(wildcard $(dir)/$(subDirName)/src/*.$(ext))	\
								)													\
							)														\
						)															\
				 	)

# check if file/directory exists
EXIST = $(if $(wildcard $(1)),true,)

# create directory, translate path separator from '/' to '\' when on windows
ifeq ($(PLATFORM),windows32)
MKDIR = $(shell mkdir $(subst /,\,$(1)))
else
MKDIR = $(shell mkdir -p $(1))
endif

# create all directorys for a list
MKDIRLIST = $(foreach dir,$(1),										\
				$(if $(call EXIST,$(dir)),,$(call MKDIR,$(dir)))	\
			)

# translate the path to a source file to a relative path to the
# corresponding file in the build directory
# does not change the extension
GEN_REL_PATH = $(addprefix $(CONFIG_DIR)/,							\
					$(subst $(CURDIR),,								\
						$(subst $(RESOURCE_LOC),_RESOURCE_,$(1))	\
					)												\
				)

###############################################################################
# setup configuration

SELECTED_CONFIG_LIST := $(or $(MAKECMDGOALS), $(DEFAULT_CONFIG))
# non empty if a clean was requested
DO_CLEAN := $(foreach word,$(SELECTED_CONFIG_LIST),$(call eq,clean,$(word)))
# selected configuration
SELECTED_CONFIG := $(filter-out clean,$(SELECTED_CONFIG_LIST))

# check, that at most one target was specified
$(if $(word 2,$(SELECTED_CONFIG)),$(error multiple targets not supported),)

CONFIG_DIR_NAME := $(SELECTED_CONFIG)
CONFIG_DIR := $(BUILD_DIR)/$(CONFIG_DIR_NAME)
TARGET := $(CONFIG_DIR)/$(TARGET_NAME)

###############################################################################
# targets, all build processes start here

.PHONY: SETUP_BUILD_DIR $(SELECTED_CONFIG) clean

# define target for selected configuration
ifneq ($(SELECTED_CONFIG),)
$(SELECTED_CONFIG): $(TARGET)
	@echo "build configuration = $(SELECTED_CONFIG)"
endif

# clean build directory if requested
ifneq ($(DO_CLEAN),)
$(info clean build directory)
$(shell $(RM) -r $(BUILD_DIR))

# dummy target for clean to avoid warning when makefile is called with clean target
clean:
	@echo build directory cleaned before build
endif

###############################################################################
# collect source files

CXX_FILES := $(call FIND_SOURCE_FILES, $(CXX_EXTENSIONS), $(CXX_SOURCE_PATHS), $(CXX_SUB_SOURCE_PATHS))
C_FILES   := $(call FIND_SOURCE_FILES, $(C_EXTENSIONS),   $(C_SOURCE_PATHS),   $(C_SUB_SOURCE_PATHS))

# collection of all sourcefiles (C, C++ and Asm)
SRC_FILES := $(CXX_FILES) $(C_FILES)

###############################################################################
# helper variables

# list of all subdirectories required for the build
BUILD_DIRECTORIES := $(call GEN_REL_PATH, $(dir $(SRC_FILES)))

# change the file extension of a file
SET_FILE_EXTENSION = $(addsuffix $(2), $(basename $(1)))
# find the location of the object file that belongs to a given source file
GEN_OBJ_PATH = $(call SET_FILE_EXTENSION, $(call GEN_REL_PATH, $(1)),.o)
# find the location of the .d file that belongs to a given source file
GEN_DEP_PATH = $(call SET_FILE_EXTENSION, $(call GEN_REL_PATH, $(1)),.d)

# list of all object files
OBJ_FILES := $(call GEN_OBJ_PATH, $(SRC_FILES))
# list of all dependency files
DEP_FILES := $(call GEN_DEP_PATH, $(SRC_FILES))

###############################################################################
# build all sources

# create the build/config directory and add new sub directories if required
SETUP_BUILD_DIR:
	@$(call MKDIRLIST, $(CONFIG_DIR))
	@$(call MKDIRLIST, $(dir $(TARGET)))
	@$(call MKDIRLIST, $(BUILD_DIRECTORIES))

#######################################
# Linker

$(TARGET): $(OBJ_FILES)
	@echo "link objects"
	$(LINKER_COMMAND)

#######################################
# dependencies

-include $(DEP_FILES)

#######################################
# C++	generate rules for all C++ Sources

define CXX_RULE
$(call GEN_OBJ_PATH,$(_SRC_FILE_PATH)): $(_SRC_FILE_PATH) | SETUP_BUILD_DIR
	$$(CXX) -o $$@ $$(DYNAMIC_LIB_ARG) $$(CXXFLAGS) $$(addprefix -D,$$(CXX_DEFINES)) $$(addprefix -I,$$(CXX_INCLUDE_PATHS)) $$(addprefix -isystem,$$(CXX_SYSTEM_INCLUDE_PATHS)) -MMD -c $$<
endef

$(foreach _SRC_FILE_PATH,$(CXX_FILES),	\
	$(eval $(CXX_RULE))					\
)

#######################################
# C		generate rules for all C Sources

define C_RULE
$(call GEN_OBJ_PATH,$(_SRC_FILE_PATH)): $(_SRC_FILE_PATH) | SETUP_BUILD_DIR
	$$(CC) -o $$@ $$(DYNAMIC_LIB_ARG) $$(CFLAGS) $$(addprefix -D,$$(C_DEFINES)) $$(addprefix -I,$$(C_INCLUDE_PATHS)) $$(addprefix -isystem,$$(C_SYSTEM_INCLUDE_PATHS)) -MMD -c $$<
endef

$(foreach _SRC_FILE_PATH,$(C_FILES),	\
	$(eval $(C_RULE))					\
)

#######################################
# Assembler - TODO

