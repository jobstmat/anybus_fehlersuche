###############################################################################
# define target platform, used to set compiler and linker

# select target platform: A53_Linux or R5_Baremetal
TARGET_PLATFORM := A53_Linux

# select name of build target
TARGET_NAME := AnyBus_Fehlersuche

# select type of build target: EXECUTABLE, STATIC_LIBRARY or DYNAMIC_LIBRARY
TARGET_TYPE := EXECUTABLE

###############################################################################

# global settings

LDLIBS := mr-udpcontrolclient

SYSROOT := ${OECORE_TARGET_SYSROOT}

BUILD_DIR := build

# default target configuration, used if nothing is specified in command line
DEFAULT_CONFIG := clean release

###############################################################################
# configuration specific settings

debug   : CXXFLAGS := -Wall -pedantic -Wextra -O0 -g3 -std=c++17
debug   : CFLAGS   := -Wall -pedantic -Wextra -O0 -g3
debug   : LDFLAGS  := --sysroot=$(SYSROOT)

release : CXXFLAGS := -Wall -pedantic -O3 -g0 -std=c++17 -flto
release : CFLAGS   := -Wall -pedantic -O3 -g0 -flto
release : LDFLAGS  := -O3 -flto -s --sysroot=$(SYSROOT)

sysroot : CXXFLAGS := -Wall -pedantic -O3 -g0 -std=c++17 -flto
sysroot : CFLAGS   := -Wall -pedantic -O3 -g0 -flto
sysroot : LDFLAGS  := -O3 -flto -s --sysroot=$(SYSROOT)

###############################################################################
# include / source paths

# defined in Project Properties/C++Build/Environmet
ifdef MR_DEVELOPMENT
	# desktop specific paths
	
	LINUX_INCLUDES := $(RESOURCE_LOC)/localDevelopment/eali-external-include
	COMMON_INCLUDES := $(RESOURCE_LOC)/localDevelopment/easc-external-include
else
	# build server specific paths
	RESOURCE_LOC := ${SYSROOT}
	
	LINUX_INCLUDES := $(SYSROOT)/usr/share/linux-external-include
	COMMON_INCLUDES := $(SYSROOT)/usr/share/common-external-include
endif

#######################################
# C++

CXX_INCLUDE_PATHS := $(COMMON_INCLUDES)				\
					 $(COMMON_INCLUDES)/_general	\
					 $(LINUX_INCLUDES)				\
					 $(LINUX_INCLUDES)/_general		\
					 $(SYSROOT)/usr/include			\
					 Implementation					\
					 Implementation/Helpers

CXX_SOURCE_PATHS :=  $(COMMON_INCLUDES)				\
					 $(COMMON_INCLUDES)/_general	\
					 $(LINUX_INCLUDES)				\
					 $(LINUX_INCLUDES)/_general		\
					 Implementation					\
					 Implementation/Helpers

CXX_SUB_SOURCE_PATHS := SharedMemory		\
						MemoryDevice		\
						GlobalMutex

CXX_DEFINES :=#

CXX_EXTENSIONS  := cc cpp

#######################################
# C

C_INCLUDE_PATHS :=#

C_SOURCE_PATHS  :=#

C_SUB_SOURCE_PATHS :=#

C_DEFINES 		:=#

C_EXTENSIONS    := c

#######################################
# Assembler - not yet implemented

###############################################################################
# make logic is implemented in a separate file
include build.mk