/*

#include "Functions.hpp"
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <mr-udpcontrolclient.hpp>

using namespace std;



static char* virtAddrPtrsChar[0x1000];
static short* virtAddrPtrsShort[0x800];



int main(int argc, char* argv[])
{
	// **********************************************************************************************
	// * Variables																					*
	// **********************************************************************************************
	bool read = false;
	bool write = false;
	bool consoleOut = false;
	bool stopIfFail = false;
	bool fillAll = false;
	bool flushAll = false;
	bool newCommandRec = false;
	int writeLengthBytes = 1;
	int numberOfReadLoops = 1;
	int numberAddresses = 8;
	int i2B = 0;
	char memValuesChar[0x2000];
	ushort memValuesShort[0x800];

	float uDPReceive[11];
	int ctr = 0;



	// **********************************************************************************************
	// * Instance Classes																			*
	// **********************************************************************************************
	CUdpControlClient UDPControl = CUdpControlClient();



	// **********************************************************************************************
	// * Let´s Start																				*
	// **********************************************************************************************
	cout << uppercase << hex;
	cout << endl << PYELLOW << "#################### MemTest Started ####################" << PRES << endl;

	// connect to udp *******************************************************************************
	UDPControl.autoConnect("MemTest", 1000);
	UDPControl.send("MemTest Started");

	// Generate Memory Values ***********************************************************************
	GenerateMemoryValues_Char(memValuesChar);
	GenerateMemoryValues_Short(memValuesShort);




	while(true) {
		//Timing
		usleep(500000);

		// Check for new UDP Commands ***************************************************************
		int recResult = UDPControl.receive(uDPReceive);
		if (recResult == 0) {
			newCommandRec = true;
		}

		// Generate Memory Values *******************************************************************
		if (newCommandRec && uDPReceive[0] == 1) {
			GenerateMemoryValues_Char(memValuesChar);
			GenerateMemoryValues_Short(memValuesShort);
		}



		// **********************************************************************************************
		// * Get virtual addresses																		*
		// **********************************************************************************************
		cout << PYELLOW << "********** Get Virtual Addresses **********" << PRES << endl;
		if (writeLengthBytes == 1) { cout << PYELLOW << "---------- Used 1 Byte Addresses ----------" << PRES << endl; }
		if (writeLengthBytes == 2) { cout << PYELLOW << "---------- Used 2 Byte Addresses ----------" << PRES << endl; }

		for (int i = 0; i < 0x1000; i++) {
			virtAddrPtrsChar[i] = GetVirtualAddressPtr_Char(BASE_ADDRESS, i, PAGE_SIZE);
			if (virtAddrPtrsChar[i] == NULL) {
				cout << PRED << "Address 0x" << BASE_ADDRESS+i << " = Get 1 Byte Virtual Address-Pointer Failed" << PRES << endl;
				exit (EXIT_FAILURE);
			}
			else {
				if (writeLengthBytes == 1 && i < numberAddresses) {
					cout << "Address 0x" << BASE_ADDRESS+i << " = " << (int*)virtAddrPtrsChar[i] << endl;
				}
			}
		}

		i2B = 0;
		for (int i = 0; i < 0x0800; i++) {
			virtAddrPtrsShort[i] = GetVirtualAddressPtr_Short(BASE_ADDRESS, i2B, PAGE_SIZE);
			if (virtAddrPtrsShort[i] == NULL) {
				cout << PRED << "Address 0x" << BASE_ADDRESS+i2B << " = Get 2 Byte Virtual Address-Pointer Failed" << PRES << endl;
				exit (EXIT_FAILURE);
			}
			else {
				if (writeLengthBytes == 2 && i < numberAddresses) {
					cout << "Address 0x" << BASE_ADDRESS+i2B << " = " << virtAddrPtrsShort[i] << endl;
				}
			}
			i2B+=2;
		}








		// Stop Program *****************************************************************************
		if (newCommandRec && uDPReceive[0] == 10) {
			cout << PYELLOW << "#################### MemTest Stopped ####################" << PRES << endl;
			exit(EXIT_SUCCESS);
		}
		newCommandRec = false;
	}


/*

	// **********************************************************************************************
	// * Check & Set Arguments																		*
	// **********************************************************************************************
	for (int i = 1; i<argc; i++) {

		if ( (string)argv[i] == "-r" ) {
			read = true;
		}
		else if ( (string)argv[i] == "-w") {
			write = true;
		}
		else if ( (string)argv[i] == "-cout") {
			consoleOut = true;
		}
		else if ( (string)argv[i] == "-stop") {
			stopIfFail = true;
		}
		else if ( (string)argv[i] == "-fill") {
			fillAll = true;
		}
		else if ( (string)argv[i] == "-flush") {
			flushAll = true;
		}
		else if ( ((string)argv[i]).substr(0,2) == "-b" ) {
			string arg = ((string)argv[i]).substr(2);
			writeLengthBytes = atoi( arg.c_str() );
			if (writeLengthBytes == 1 || writeLengthBytes == 2) {
				//nix
			}
			else {
				cout << PRED << "Error Parameter -b | Valid Values are 1 or 2" << PRES << endl << endl;
				exit (EXIT_FAILURE);
			}
		}
		else if ( ((string)argv[i]).substr(0,2) == "-l" ) {
			string arg = ((string)argv[i]).substr(2);
			numberOfReadLoops = atoi( arg.c_str() );
		}
		else if ( ((string)argv[i]).substr(0,2) == "-a" ) {
			string arg = ((string)argv[i]).substr(2);
			numberAddresses = atoi( arg.c_str() );
		}
		else {
			cout << PYELLOW << "********** HELP **********" << endl;
			cout << "-r\tread" << endl;
			cout << "-w\twrite" << endl;
			cout << "-bxx\tbytes to write/address.. -b1 or -b2" << endl;
			cout << "-axx\taddress to write.. (decimal value).. -a1 to -a4096" << endl;
			cout << "-lxx\tnumber of loops" << endl;
			cout << "-cout\tconsole out for reads (slows down)" << endl;
			cout << "-stop\tstops if a read fails" << endl;
			cout << "-fill\tfill all addresses with 0xFF first" << endl;
			cout << "-flush\tflush all addresses with 0x00 first" << endl;
			cout << PRES;
			exit(0);
		}
	}







	// **********************************************************************************************
	// * Get virtual addresses																		*
	// **********************************************************************************************
	cout << PYELLOW << "********** Get Virtual Addresses **********" << PRES << endl;
	if (writeLengthBytes == 1) { cout << PYELLOW << "---------- Used 1 Byte Addresses ----------" << PRES << endl; }
	if (writeLengthBytes == 2) { cout << PYELLOW << "---------- Used 2 Byte Addresses ----------" << PRES << endl; }

	for (int i = 0; i < 0x1000; i++) {
		virtAddrPtrsChar[i] = GetVirtualAddressPtr_Char(BASE_ADDRESS, i, PAGE_SIZE);
		if (virtAddrPtrsChar[i] == NULL) {
			cout << PRED << "Address 0x" << BASE_ADDRESS+i << " = Get 1 Byte Virtual Address-Pointer Failed" << PRES << endl;
			exit (EXIT_FAILURE);
		}
		else {
			if (writeLengthBytes == 1 && i < numberAddresses) {
				cout << "Address 0x" << BASE_ADDRESS+i << " = " << (int*)virtAddrPtrsChar[i] << endl;
			}
		}
	}

	i2B = 0;
	for (int i = 0; i < 0x0800; i++) {
		virtAddrPtrsShort[i] = GetVirtualAddressPtr_Short(BASE_ADDRESS, i2B, PAGE_SIZE);
		if (virtAddrPtrsShort[i] == NULL) {
			cout << PRED << "Address 0x" << BASE_ADDRESS+i2B << " = Get 2 Byte Virtual Address-Pointer Failed" << PRES << endl;
			exit (EXIT_FAILURE);
		}
		else {
			if (writeLengthBytes == 2 && i < numberAddresses) {
				cout << "Address 0x" << BASE_ADDRESS+i2B << " = " << virtAddrPtrsShort[i] << endl;
			}
		}
		i2B+=2;
	}



	// **********************************************************************************************
	// * Write to Memory																			*
	// **********************************************************************************************

	cout << PYELLOW << "********** Writing to Memory **********" << PRES << endl;

	// fill *************************************************************************************
	if (fillAll && writeLengthBytes == 1) {
		cout << PYELLOW << "---------- Fill address 0x0000..0x0FFF with 0xFF ----------" << PRES << endl;
		char val = 0xFF;
		for (int i = 0; i < 0x1000; i++) { *virtAddrPtrsChar[i] = val; }
		cout << "Done" << endl;
	}

	if (fillAll && writeLengthBytes == 2) {
		cout << PYELLOW << "---------- Fill address 0x0000..0x0FFE(0x0FFF) with 0xFFFF ----------" << PRES << endl;
		ushort val = 0xFFFF;
		for (int i = 0; i < 0x0800; i++) { *virtAddrPtrsShort[i] = val; }
		cout << "Done" << endl;
	}

	// flush ************************************************************************************
	if (flushAll && writeLengthBytes == 1) {
		cout << PYELLOW << "---------- Flush address 0x0000..0x0FFF with 0x00 ----------" << PRES << endl;
		char val = 0x00;
		for (int i = 0; i < 0x1000; i++) { *virtAddrPtrsChar[i] = val; }
		cout << "Done" << endl;
	}

	if (flushAll && writeLengthBytes == 2) {
		cout << PYELLOW << "---------- Flush address 0x0000..0x0FFE(0x0FFF) with 0x0000 ----------" << PRES << endl;
		ushort val = 0x0000;
		for (int i = 0; i < 0x0800; i++) { *virtAddrPtrsShort[i] = val; }
		cout << "Done" << endl;
	}

	// Data *************************************************************************************
	if (write) {
		cout << PYELLOW << "---------- Writing Values ----------" << PRES << endl;

		if (writeLengthBytes == 1) {
			for (int i = 0; i < numberAddresses; i++) {
				cout << "0x" << BASE_ADDRESS+i << " <- 0x" << (int)memValuesChar[i] << endl;
				*virtAddrPtrsChar[i] = memValuesChar[i];
			}
		}

		if (writeLengthBytes == 2) {
			i2B = 0;
			for (int i = 0; i < numberAddresses; i++) {
				cout << "0x" << BASE_ADDRESS+i2B << " <- 0x" << memValuesShort[i] << endl;
				*virtAddrPtrsShort[i] = memValuesShort[i];
				i2B+=2;
			}
		}
	}



	// **********************************************************************************************
	// * Read from Memory																			*
	// **********************************************************************************************
	int passCounter = 0;
	int failCounter = 0;
	float percentFinished = 0;

	if (read) {
		cout << PYELLOW << "********** Reading from Memory **********" << PRES << endl;

		for (int j = 0; j < numberOfReadLoops; j++) {
			percentFinished = ((float)j+1)/(float)numberOfReadLoops*100;
			if (consoleOut) {
				cout << dec << PYELLOW  << "---------- Reading Loop " << j+1 << " / " << numberOfReadLoops  << " | " << percentFinished << "% ----------" << PRES << hex << endl;
			}
			else {
				if ( j % (4096000/numberAddresses) == 0 || j == numberOfReadLoops-1) { cout << dec << PYELLOW << "---------- Reading Loop " << j+1 << " / " << numberOfReadLoops << " | " << percentFinished << "% ----------" << PRES << hex << "\t\t\r" << flush; }
			}

			if (writeLengthBytes == 1) {
				int readResult = 0x0;
				for (int i = 0; i < numberAddresses; i++) {
					readResult = *virtAddrPtrsChar[i];
					if (consoleOut) { cout << "0x" << BASE_ADDRESS+i << " -> " << (ulong*)virtAddrPtrsChar[i] << " = S 0x" << (int)memValuesChar[i] << " | I 0x" << readResult << " | "; }
					if (readResult == memValuesChar[i]) {
						passCounter++;
						if (consoleOut) { cout << PGREEN << "Pass" << PRES << endl; }
					}
					else {
						failCounter++;
						if (stopIfFail && !consoleOut) { cout << "0x" << BASE_ADDRESS+i << " -> " << (ulong*)virtAddrPtrsChar[i] << " = S 0x" << (int)memValuesChar[i] << " | I 0x" << readResult << " | "<< PRED << "Fail" << PRES << endl; }
						if (consoleOut) { cout << PRED << "Fail" << PRES << endl; }
						if (stopIfFail) {
							j = numberOfReadLoops;
							break;
						}
					}
				}
			}

			if (writeLengthBytes == 2) {
				i2B = 0;
				int readResult = 0x0;
				for (int i = 0; i < numberAddresses; i++) {
					readResult = *virtAddrPtrsShort[i] & 0x0000FFFF;
					if (consoleOut) { cout << "0x" << BASE_ADDRESS+i2B << " -> " << (ulong*)virtAddrPtrsShort[i] << " = S 0x" << memValuesShort[i] << " | I 0x" << readResult << " | "; }
					if (readResult == memValuesShort[i]) {
						passCounter++;
						if (consoleOut) { cout << PGREEN << "Pass" << PRES << endl; }
					}
					else {
						failCounter++;
						if (stopIfFail && !consoleOut) { cout << "0x" << BASE_ADDRESS+i2B << " -> " << (ulong*)virtAddrPtrsShort[i] << " = S 0x" << (int)memValuesShort[i] << " | I 0x" << readResult << " | "<< PRED << "Fail" << PRES << endl; }
						if (consoleOut) { cout << PRED << "Fail" << PRES << endl; }
						if (stopIfFail) {
							j = numberOfReadLoops;
							break;
						}
					}
				i2B+=2;
				}
			}
		}
	}



	// **********************************************************************************************
	// * Ende Gelände																				*
	// **********************************************************************************************
	cout << endl << PYELLOW  << "********** Finished **********" << PRES << endl;
	cout << dec << "Result : " << PGREEN << passCounter << " Pass " << PRES << " | " << PRED << failCounter << " Fail " << PRES << endl << endl;

}
*/
