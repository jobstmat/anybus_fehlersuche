
#include "Functions.hpp"
#include "Helpers.h"

#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <unistd.h>
#include <random>
#include <iomanip>
#include <map>

using namespace std;



#define PRED "\033[1;31m"	// ANSI code for red font
#define PGREEN "\033[1;32m" // ANSI code for green font
#define PYELLOW "\033[1;33m" // ANSI code for yellow font
#define PRES "\033[0m"	// ANSI code to reset font

#define HEADERLENGTH 80 // Length for Headerlines



// UDP Client Library einbinden
#include <mr-udpcontrolclient.hpp>

int main(int argc, char* argv[])
{
	map<ulong, ushort*> allAdresses; // Physical Address | Virtual Address Pointer
	map<ulong, ushort> testValues; // Physical Address | Test Value

	//bool read = false;
	bool writeOnce = false;
	bool writeBeforeRead = false;
	bool consoleOut = false;
	bool stopIfFail = false;
	bool fillAll = false;
	bool flushAll = false;
	bool xUsed = false;
	bool coutFailOnly = false;

	int numberOfTestLoops = 1;
	int numberOfReadLoops = 1;
	int startAddressOffest = 0x0000;
	int stopAddressOffset = 0x00FE;

	uint pauseBetweenRead = 0;
	ushort xAddrTestValue = 0x0000;
	ulong xAddr = 0x0000;

	CUdpControlClient UDPControlExample = CUdpControlClient();



	// **********************************************************************************************
	// * Check & Set Arguments																		*
	// **********************************************************************************************
	for (int i = 1; i<argc; i++) {

		if ( (string)argv[i] == "-r" ) {
			//read = true;
		}
		else if ( (string)argv[i] == "-wo") {
			writeOnce = true;
		}
		else if ( (string)argv[i] == "-wbr") {
			writeBeforeRead = true;
		}
		else if ( (string)argv[i] == "-cout") {
			consoleOut = true;
		}
		else if ( (string)argv[i] == "-sf") {
			stopIfFail = true;
		}
		else if ( (string)argv[i] == "-fo") {
			coutFailOnly = true;
		}
		else if ( (string)argv[i] == "-fill") {
			fillAll = true;
		}
		else if ( (string)argv[i] == "-flush") {
			flushAll = true;
		}
		else if ( ((string)argv[i]).substr(0,4) == "-tl=" ) {
			string arg = ((string)argv[i]).substr(4);
			numberOfTestLoops = atoi( arg.c_str() );
		}
		else if ( ((string)argv[i]).substr(0,4) == "-rl=" ) {
			string arg = ((string)argv[i]).substr(4);
			numberOfReadLoops = atoi( arg.c_str() );
		}
		else if ( ((string)argv[i]).substr(0,7) == "-pause=" ) {
			string arg = ((string)argv[i]).substr(7);
			pauseBetweenRead = atoi( arg.c_str() );
		}
		else if ( ((string)argv[i]).substr(0,7) == "-start=" ) {
			string arg = ((string)argv[i]).substr(7);
			startAddressOffest = stoul( arg.c_str(), nullptr, 16 );
		}
		else if ( ((string)argv[i]).substr(0,6) == "-stop=" ) {
			string arg = ((string)argv[i]).substr(6);
			stopAddressOffset = stoul( arg.c_str(), nullptr, 16 );
		}
		else if ( ((string)argv[i]).substr(0,4) == "-xa=" ) {
			string arg = ((string)argv[i]).substr(4);
			xAddr = stoul( arg.c_str(), nullptr, 16 );
			xUsed = true;
		}
		else if ( ((string)argv[i]).substr(0,4) == "-xd=" ) {
			string arg = ((string)argv[i]).substr(4);
			xAddrTestValue = stoul( arg.c_str(), nullptr, 16 );
		}
		else {
			cout << PYELLOW << "******************** HELP ********************" << endl;
			//cout << "-r\tread" << endl;
			cout << "-wo\twrite once to memory" << endl;
			cout << "-wbr\twrite everytime to memory before read" << endl;
			cout << "-start=0xXXXX\tTest start address (hex value)" << endl;
			cout << "-stop=0xXXXX\tTest stop address (hex value)" << endl;
			cout << "-xa=0xXXXX\twrite value to single address (hex value)" << endl;
			cout << "-xd=0xXXXX\tdata to write for -xa (hex value)" << endl;
			cout << "-rl=XX\tnumber of read-loops (read same address x-times in a loop)" << endl;
			cout << "-tl=XX\tnumber of test-loops" << endl;
			cout << "-pause=XX\tPause between read (in µs)" << endl;
			cout << "-cout\tconsole out for reads (slows down)" << endl;
			cout << "-sf\tstops if a read fails" << endl;
			cout << "-fill\tfill all addresses with 0xFFFF first (0x0000..0x1FFF)" << endl;
			cout << "-flush\tflush all addresses with 0x0000 first (0x0000..0x1FFF)" << endl;
			cout << PRES;
			exit(0);
		}
	}



	// **********************************************************************************************
	// * Instance Classes																			*
	// **********************************************************************************************
	CUdpControlClient UDPControlDesc = CUdpControlClient();
	CUdpControlClient UDPControlParam = CUdpControlClient();
	CUdpControlClient UDPControlCtrl = CUdpControlClient();



	// **********************************************************************************************
	// * Let´s Start																				*
	// **********************************************************************************************
	cout << endl << PYELLOW << createHeaderline("AnyBus Memory Test", HEADERLENGTH, '#', '#', '\n') << PRES << endl;
	// cout << uppercase << hex << setfill('0') << setw(4);



	// **********************************************************************************************
	// * Get addresses																				*
	// **********************************************************************************************
	cout << PYELLOW << createHeaderline("Get Virtual Addresses - Please Wait", HEADERLENGTH, '*', '*', '\n') << PRES;

	for (int i = 0; i < 0x4000; i+=2) {

		ulong physAddr = BASE_ADDRESS + i;
		ushort* virtAddr = GetVirtualAddressPtr_Short(BASE_ADDRESS, i, PAGE_SIZE);

		if (virtAddr == NULL) {
			cout << PRED << "Address 0x" << physAddr << " -> Get Virtual Address Failed" << PRES << endl;
			exit (EXIT_FAILURE);
		}
		else {
			allAdresses [physAddr] = virtAddr;
		}
	}
	cout << "Done" << endl;



	// **********************************************************************************************
	// * Generate Test Values																		*
	// **********************************************************************************************
	cout << PYELLOW << createHeaderline("Generate Test Values - Please wait", HEADERLENGTH, '*', '*', '\n') << PRES;
	cout << uppercase << hex << setfill('0') << setw(4);

	for (int i = 0x0000; i <= 0x3FFF; i+=2) {
		ulong physTestAddr = BASE_ADDRESS + i;
		testValues [physTestAddr] = rand()&0xFFFF;
		//testValues [physTestAddr] = 0x0000;
	}

	if (xUsed) {
		testValues [BASE_ADDRESS + xAddr] = xAddrTestValue;
	}

//	testValues [BASE_ADDRESS + 0] = xAddrTestValue;
//	testValues [BASE_ADDRESS + 2] = 0xFFFF;
//	testValues [BASE_ADDRESS + 4] = 0xFFFF;
//	testValues [BASE_ADDRESS + 6] = 0x0000;
//	testValues [BASE_ADDRESS + 8] = 0x0000;
//	testValues [BASE_ADDRESS + 0x0010] = 0xFFFF;

	cout << "Done" << endl;



	// **********************************************************************************************
	// * Flush / Fill Memory																		*
	// **********************************************************************************************
	// fill *****************************************************************************************
	if (fillAll) {
		cout << PYELLOW << createHeaderline("Fill address 0x0000..0x0FFE(0x0FFF) with 0xFFFF", HEADERLENGTH, '*', '*', '\n') << PRES;

		for (int i = 0; i < 0x0FFF; i+=2) {
			ushort* virt = allAdresses[BASE_ADDRESS + i];
			*virt = (ushort)0xFFFF;
		}
		cout << "Done" << endl;
	}

	// flush ****************************************************************************************
	if (flushAll) {
		cout << PYELLOW << createHeaderline("Flush address 0x0000..0x0FFE(0x0FFF) with 0x0000", HEADERLENGTH, '*', '*', '\n') << PRES;

		for (int i = 0; i < 0x0FFF; i+=2) {
			ushort* virt = allAdresses[BASE_ADDRESS + i];
			*virt = (ushort)0x0000;
		}
		cout << "Done" << endl;
	}



	// **********************************************************************************************
	// * Read / Write Memory																		*
	// **********************************************************************************************

	// write test values once ***********************************************************************
	if (writeOnce) {
		for (int i = startAddressOffest; i <= stopAddressOffset; i+=2) {
			ulong physTestAddr = BASE_ADDRESS + i;
			*allAdresses[physTestAddr] = testValues[physTestAddr];
		}
	}

	// Testloop *************************************************************************************
	double passCounter = 0;
	double failCounter = 0;
	double currentTestNumber = 0;
	double numberOfTests = (double)(numberOfTestLoops*numberOfReadLoops*((stopAddressOffset-startAddressOffest)/2));
	double percentFinished = 0;
	ushort readResult = 0xDEAD;
	ulong physTestAddr = 0x81610000;

	cout << PYELLOW << createHeaderline("Testing", HEADERLENGTH, '*', '*', '\n') << PRES;

	for (int j = 0; j < numberOfTestLoops; j++) {

		if (consoleOut) {
			stringstream HeaderMidPart;
			HeaderMidPart << dec << "Test Loop " << j+1 << " / " << numberOfTestLoops << " | " << fixed << setprecision(2) << percentFinished << "%";
			cout << PYELLOW << createHeaderline(HeaderMidPart.str(), HEADERLENGTH, '-', '-', '\n') << PRES;
		}

		for (int y = startAddressOffest; y <= stopAddressOffset; y+=2) {
			physTestAddr = BASE_ADDRESS + y;

			if (writeBeforeRead) { *allAdresses[physTestAddr] = testValues[physTestAddr]; }

			for (int i = 0; i < numberOfReadLoops; i++) {
				usleep(pauseBetweenRead);

				percentFinished = currentTestNumber/numberOfTests*100;

				readResult = *allAdresses[physTestAddr];
				if (readResult == testValues[physTestAddr]) {
					passCounter++;
					if (consoleOut &! coutFailOnly) { cout << "0x" << physTestAddr << " -> " << allAdresses[physTestAddr] << " = S 0x" << setfill('0') << setw(4) << testValues[physTestAddr] << " | I 0x" << setfill('0') << setw(4) << readResult << " | " << PGREEN << "Pass" << PRES << endl; }
				}
				else {
					failCounter++;
					//if (stopIfFail &! consoleOut) { cout << "0x" << physTestAddr << " -> " << allAdresses[physTestAddr] << " = S 0x" << setfill('0') << setw(4) << testValues[physTestAddr] << " | I 0x" << setfill('0') << setw(4) << readResult << " | "<< PRED << "Fail" << PRES << endl; }
					if (consoleOut) { cout << "0x" << physTestAddr << " -> " << allAdresses[physTestAddr] << " = S 0x" << setfill('0') << setw(4) << testValues[physTestAddr] << " | I 0x" << setfill('0') << setw(4) << readResult << " | " << PRED << "Fail" << PRES << endl; }
				}

				if ( !consoleOut && (((ulong)currentTestNumber % 10000 == 0) || currentTestNumber == 0) ) {
					cout << fixed << setprecision(0) << PYELLOW << "Test " << currentTestNumber << " / " << numberOfTests << setprecision(2) << " (" << percentFinished << "%)" << " --> " << setprecision(0) << PGREEN << passCounter << " Pass" << PYELLOW << " | " << PRED << failCounter << " Fail (" << setprecision(2) << 100*failCounter/(failCounter+passCounter) << "%)" << PRES << "\t\t\t\t\r" << flush;
					if (stopIfFail &! consoleOut) { cout << endl; }
				}
				currentTestNumber += 1;
				if (stopIfFail && failCounter > 0) { goto endTest; }
			}
		}
	}



	// **********************************************************************************************
	// * Ende Gelände																				*
	// **********************************************************************************************
	endTest:
	cout << PYELLOW << createHeaderline("Result", HEADERLENGTH, '*', '*', '\n') << PRES;
	cout << dec << fixed << setprecision(0) << PGREEN << passCounter << " Pass" << PYELLOW << " | " << PRED << failCounter << " Fail (" << setprecision(2) << 100*failCounter/(failCounter+passCounter) << "%)" << PRES << endl;
	cout << PYELLOW << createHeaderline("Test Finished", HEADERLENGTH, '*', '*', '\n') << PRES << endl;
}



