/*
 * CreateHeaderline.cc
 *
 *  Created on: 04.02.2020
 *      Author: jobstmat
 */
/*
#####################################################################################################
# Creates a Headerline with leading and trailing symbols with a selectable length					#
#####################################################################################################
*/

#include <Helpers.h>

using namespace std;



string createHeaderline(string midpart, uint length, char fillLeading, char fillTrailing, char lineFeed)
{
	string formattedHeaderline = "";

	if (midpart.size() < length)
	{
		// Insert 1 Space leading and trailing if not empty (if empty = solid line)
		if (midpart.size() != 0)
		{
			formattedHeaderline = " " + midpart + " ";
		}

		// evaluate number of symbols to insert
		int fillLength = ( length - formattedHeaderline.size() ) /2;

		// fill
		string fillString = "";
		fillString.resize(fillLength, fillLeading); // generate leading
		formattedHeaderline = fillString + formattedHeaderline; // fill leading
		formattedHeaderline.resize(length, fillTrailing); // fill trailing
		formattedHeaderline += lineFeed;
	}

	return formattedHeaderline;
}



