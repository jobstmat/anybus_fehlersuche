/*
 * Helpers.h
 *
 *  Created on: 12.02.2020
 *      Author: jobstmat
 */
/*
#####################################################################################################
# Little Heper Functions for data formatting, etc													#
#####################################################################################################
*/
#ifndef SRC_HELPERS_HELPERS_H_
#define SRC_HELPERS_HELPERS_H_



#include <string.h>
#include <iostream>



// CreateHedaerline. Creates a Headerline with leading and trailing symbols with a selectable length.
std::string createHeaderline(std::string midpart, uint length, char fillLeading, char fillTrailing, char lineFeed);

//// GetTimeString. Returns System Time as String "HH:MM:SS"
//std::string getTimeString();
//
//// BoolToString. Converts a boolen value to a True False String.
//std::string boolToString (bool b);
//
//// NumToString0. Format Numeric to String with leading 0.
//std::string formatNumToString (int value, uint length, uint precision, char fill);
//std::string formatNumToString (uint value, uint length, uint precision, char fill);
//std::string formatNumToString (ulong value, uint length, uint precision, char fill);
//std::string formatNumToString (float value, uint length, uint precision, char fill);
//std::string formatNumToString (std::string value, uint length, uint precision, char fill);
//
//// Set system date and time from UDP
//void setDateTime(char puffer[11]);



#endif /* SRC_HELPERS_HELPERS_H_ */
