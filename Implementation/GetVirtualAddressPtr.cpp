/*
 * GetVirtualAddressPtr.cpp
 *
 *  Created on: 11.03.2022
 *      Author: jobstmat
 */
// ##################################################################################################
// # Get Virtual Address																			#
// # This function gets the mapped virtual address (pointer) from a physical address				#
// # Read about Linux memory management, paging and mmap for further information					#
// #																								#
// # input  : - base address																		#
// #		: - address offset																		#
// #		: - page size																			#
// # return : - virtual address pointer or NULL if Error											#
// ##################################################################################################

#include "Functions.hpp"
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

using namespace std;



char* GetVirtualAddressPtr_Char (ulong baseAddress, ulong baseAddressOffset, ulong pageSize)
{
	ulong pageAddress = baseAddress &~ pageSize;
	char* virtualAddressPtr = NULL; // initalize with 0 = error;

	// Open File ************************************************************************************
	int fd = open("/dev/mem", O_RDWR);

	// Get Virtual Address Pointer ******************************************************************
	if (fd >= 1) // file open successfully
	{
		// mmap -> get pointer ----------------------------------------------------------------------
		virtualAddressPtr = (char*)mmap(NULL, pageSize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, pageAddress);
		if (virtualAddressPtr == MAP_FAILED) // Address read error ----------------------------------
		{
			virtualAddressPtr = NULL;
		}
		else // Address read successful, unmap and calculate address --------------------------------
		{
			munmap(&virtualAddressPtr, pageSize);
			virtualAddressPtr += baseAddressOffset/sizeof(char); // Pointer Base Address + Offset
		}

		// Close file *******************************************************************************
		close(fd);
	}

	// Return ***************************************************************************************
	return virtualAddressPtr;
}



ushort* GetVirtualAddressPtr_Short (ulong baseAddress, ulong baseAddressOffset, ulong pageSize)
{
	ulong pageAddress = baseAddress &~ pageSize;
	ushort* virtualAddressPtr = NULL; // initalize with 0 = error;

	// Open File ************************************************************************************
	int fd = open("/dev/mem", O_RDWR);

	// Get Virtual Address Pointer ******************************************************************
	if (fd >= 1) // file open successfully
	{
		// mmap -> get pointer ----------------------------------------------------------------------
		virtualAddressPtr = (ushort*)mmap(NULL, pageSize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, pageAddress);
		if (virtualAddressPtr == MAP_FAILED) // Address read error ----------------------------------
		{
			virtualAddressPtr = NULL;
		}
		else // Address read successful, unmap and calculate address --------------------------------
		{
			munmap(&virtualAddressPtr, pageSize);
			virtualAddressPtr += baseAddressOffset/sizeof(short); // Pointer Base Address + Offset
		}

		// Close file *******************************************************************************
		close(fd);
	}

	// Return ***************************************************************************************
	return virtualAddressPtr;
}
