/*
 * Functions.hpp
 *
 *  Created on: 11.01.2022
 *      Author: jobstmat
 */
/*
#####################################################################################################
# Functions																							#
#####################################################################################################
*/



#ifndef SRC_FUNCTIONS_H_
#define SRC_FUNCTIONS_H_

#include <stdlib.h>

using namespace std;


#define BASE_ADDRESS 0x81610000
#define BASE_ADDRESS_OFFSET 0x0
#define PAGE_SIZE 0x4000


#define PRED "\033[1;31m"	// ANSI code for red font
#define PGREEN "\033[1;32m" // ANSI code for green font
#define PYELLOW "\033[1;33m" // ANSI code for yellow font
#define PRES "\033[0m"	// ANSI code to reset font


// Get Virtual Address
char* GetVirtualAddressPtr_Char (ulong baseAddress, ulong baseAddressOffset, ulong pageSize);
ushort* GetVirtualAddressPtr_Short (ulong baseAddress, ulong baseAddressOffset, ulong pageSize);
//char GenerateVirtualAddresses(char* virtAddrArrayPtr);
//int GenerateVirtualAddresses(ushort *virtAddrArrayPtr);


#endif /* SRC_FUNCTIONS_H_ */
